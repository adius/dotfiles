# Command Line Tools

## Information

### General

- **`man ascii`** - Print the ascii tables in octal, hexadeciamal and decimal

### Commands

- **whatis** - Search the whatis database for complete words.
- **apropos** - Search the whatis database for strings.
- **man** - Manual page.
- **tldr** - Summary of manual page.
- **bro** - Voted summaries of manual page.
- **compgen** - Bash built-in commands lister.
- **type** - Get type of command (alias, keyword, function, builtin, file).
- **cheat** - Create and view interactive cheatsheets on the command-line.


### Viewer

- **od** - Octal, decimal, hex, ASCII dump
- **strings** - find the printable strings in a object, or other binary, file
- **hexdump** - 
- **xxd** -


## Misc

- **who**
- **lsof** - List open files.
- **system_profiler** - Information about the computer hardware and settings.
- **caffeinate** - Prevent the system from sleeping on behalf of a utility.


## Duplicates

- **fdupes**
- **duff**
- **rmlint**
- **rdfind**


## Comparison

- **cmp**
- **diff**
- **colordiff**
- **icdiff** - Improved colored diff
- **diff3** - compare three files and show any differences among them
- **numdiff** - Compare files ignoring small numeric differences and different numeric formats
- **comm** - Select or reject lines common to two files
- `diff <(xxd b1) <(xxd b2)`


## File System

- **rename** - Rename multiple files
- **seq** - Print sequences of numbers
- **mkfile** - Create one or more zero padded files


## Internet

- **HTTPie** - Command line HTTP client and user-friendly cURL replacement.


## Browsers

- **lynx**
- **w3m**
- **links**
- **elinks** - Advanced and well-established feature-rich text mode web browser.
- **surfraw** - CLi to a variety of popular WWW search engines.


## Music

- **afplay** - Plays an audio file to the default audio output.
- **cursynth**
- **fluidsynth**
- **lilypond**
- **beet**


## Convert

- **json2yaml**


## Editing

- **atom**
- **mate**
- **fc** - (fix command) Edit last command with vi.
- expand - Expand tabs to spaces
- unexpand - Expand spaces to tabs


## Search

- grep - file pattern searcher
- ack - 
- ag - Recursively search for PATTERN in PATH. Like grep or ack, but faster.


## Jumper

- fasd
- autojump
- z
- v


### Spell Cheking

- aspell - Interactive spell checker
- hunspell - Spell checker, stemmer and morphological analyzer


### Weather

- wego - Weather app with ascii art (github.com/schachmat/wego)
## Links

- http://shell-fu.org - All those little command line goodies that you come across and then can never find again when you need 
