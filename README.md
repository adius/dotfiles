# Dotfiles

Installation:

```bash
curl --silent https://gitlab.com/adius/dotfiles/raw/master/install.sh | bash
```

**Attention:** The user must have sudo rights to install dependencies


## Structure

* `bin` — custom scripts
* `home` — files that are symlinked to `$HOME` directory
* `terminal` — terminal config


## Steps

1. Install Command Line Developer Tools
1. Run install script
1. Enable Shiftit
    (System Preferences > Security & Privacy > Privacy > Accessibility)


## Scripts

Jxa macOS automation scripts are symlinked from
`~/dotfiles/scripts` to `~/Library/Scripts`
and are displayed in the scripts editor submenu in the menu bar.
(Enable via
`Script Editor.app > Preferences > General > Show Script menu in menu bar`)
Add subdirectories with app names for app specific scripts.

The `symlink-dotfiles` script must be run after adding new scripts.


## Script Libraries

Script libraries are symlinked from
`~/dotfiles/script-libraries` to `~/Library/Script Libraries/dotfiles`
and can be imported like this:

```js
const tools = Library('dotfiles/tools')
```
